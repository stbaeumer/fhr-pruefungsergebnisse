﻿namespace fhr_pruefungsergebnisse
{
    internal class Schueler
    {
        public int Id { get; internal set; }
        public int Bezugsjahr { get; internal set; }
        public string Status { get; internal set; }
        public string Grade { get; internal set; }
        public string Vorname { get; internal set; }
        public string Schuljahr { get; internal set; }
        public string Jahrgang { get; internal set; }
        public string Bildungsgang { get; internal set; }
        public string Klassenzielerreicht { get; internal set; }
        public string AustrittsgrundBildungsgang { get; internal set; }
        public string Gliederung { get; internal set; }
        public string Nachname { get; internal set; }
        public string Klasse { get; internal set; }
        public string Bildungsgangname { get; internal set; }
    }
}