﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fhr_pruefungsergebnisse
{
    class Program
    {
        public const string ConnectionStringAtlantis = @"Dsn=Atlantis9;uid=DBA";

        static void Main(string[] args)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);

            List<string> datei = new List<string>();

            try
            {
                Console.WriteLine("FHR-Prüfungsergebnisse (Version 20190927)");
                Console.WriteLine("=========================================");
                Console.WriteLine("");
                Console.WriteLine("Abfrage der FHR-Prüfungsergebnisse durch die Bezirksregierung Dez. 45 (Berufskolleg)");
                Console.WriteLine("Bildungsgänge nach den Anlagen C 1 sowie C 2 der APO-BK");
                Console.WriteLine("");

                int sj = (DateTime.Now.Month >= 8 ? DateTime.Now.Year : DateTime.Now.Year - 1);

                string jahrNachAbschluss = (sj).ToString() + "/" + (sj + 1 - 2000);
                string jahrAbschluss = (sj - 1).ToString() + "/" + (sj - 2000);
                string jahrEinschulung = (sj - 2).ToString() + "/" + (sj - 1 - 2000);
                string jahrEinschulungWiederholerUnterstufe = (sj - 3).ToString() + "/" + (sj - 2 - 2000);

                datei.Add("Abfrage der FHR-Prüfungsergebnisse für Prüfungssommer " + DateTime.Now.Year);
                datei.Add("durch die Bezirksregierung Dez. 45 (Berufskolleg)");
                datei.Add("Bildungsgänge nach den Anlagen C 1 sowie C 2 der APO-BK");
                datei.Add("");
                datei.Add(System.Environment.UserName + " | " + DateTime.Now);
                datei.Add("Name der Schule:".PadRight(20) + "Berufskolleg Borken");
                datei.Add("Schulnummer:".PadRight(20) + 177659);

                Schuelers schuelers = new Schuelers(ConnectionStringAtlantis, jahrEinschulungWiederholerUnterstufe, jahrEinschulung, jahrAbschluss, jahrNachAbschluss);

                foreach (var gliederung in (from s in schuelers
                                            where s.Jahrgang == "C031"
                                            where s.Gliederung.EndsWith("0")
                                            where s.Klasse != null
                                            where s.Klasse.Length > 3
                                            select s.Gliederung).Distinct().ToList())
                {

                    List<Schueler> schuelerMitAbschluss = schuelers.GetAbsolventen(gliederung, jahrAbschluss);
                    List<Schueler> eingeschulte = schuelers.GetEingeschulte(gliederung, jahrEinschulung);
                    List<Schueler> eingeschulteWiederholer = schuelers.GetEingeschluteWiederholer(gliederung, jahrEinschulung, jahrEinschulungWiederholerUnterstufe);
                    List<Schueler> abgängerDerUnterstufe = schuelers.GetAbgängerDerUnterstufe(gliederung, jahrEinschulung, jahrAbschluss);
                    List<Schueler> wiederholerDerUnterstufe = schuelers.GetWiederholerDerUnterstufe(gliederung, jahrEinschulung, jahrAbschluss);
                    List<Schueler> wiederholerOberstufe = schuelers.GetWiederholerDerOberstufe(gliederung, jahrAbschluss, jahrNachAbschluss);

                    string bildungsgangname = (from s in schuelers where s.Gliederung == gliederung select s.Bildungsgangname).FirstOrDefault();

                    int i = 1;
                    datei.Add("");
                    string ueberschrift = "Einschulung im Jahr " + jahrEinschulung + " in der Gliederung " + gliederung + " (" + bildungsgangname + "):";
                    datei.Add(ueberschrift);
                    datei.Add("".PadRight(ueberschrift.Length,'='));

                    foreach (var schueler in eingeschulte)
                    {
                        datei.Add(i.ToString().PadLeft(4) + ". " + (schueler.Nachname + ", ").PadRight(22) + schueler.Vorname);
                        i++;
                    }
                    if (i == 1)
                    {
                        datei.Add(" -keine- ");
                    }

                    datei.Add("");

                    datei.Add("Eingeschulte Wiederholer in " + gliederung + " des vorhergehenden Schuljahes " + jahrEinschulungWiederholerUnterstufe  + " (" + bildungsgangname + "):");

                    i = 1;

                    foreach (var schueler in eingeschulteWiederholer)
                    {
                        datei.Add(i.ToString().PadLeft(4) + ". " + (schueler.Nachname + ", ").PadRight(22) + schueler.Vorname);
                        i++;
                    }
                    if (i == 1)
                    {
                        datei.Add(" -keine- ");
                    }

                    datei.Add("");

                    datei.Add("Abgänger der Unterstufe nach Gründen der Gliederung " + gliederung + " (" + bildungsgangname + "):");

                    i = 1;

                    foreach (var schueler in abgängerDerUnterstufe)
                    {
                        datei.Add(i.ToString().PadLeft(4) + ". " + (schueler.Nachname + ", ").PadRight(22) + schueler.Vorname.PadRight(17) + "Anforderungsniveau (  ) Ausbildungplatz (  ) Anderer Grund (  )");
                        i++;
                    }
                    if (i == 1)
                    {
                        datei.Add(" -keine- ");
                    }

                    datei.Add("");

                    datei.Add("Wiederholer der Unterstufe der Gliederung " + gliederung + " (" + bildungsgangname + "):");

                    i = 1;

                    foreach (var schueler in wiederholerDerUnterstufe)
                    {
                        datei.Add(i.ToString().PadLeft(4) + ". " + (schueler.Nachname + ", ").PadRight(22) + schueler.Vorname);
                        i++;
                    }
                    if (i == 1)
                    {
                        datei.Add(" -keine- ");
                    }

                    datei.Add("");

                    datei.Add("Nicht zur Prüfung im Sommer " + jahrAbschluss + " der Gliederung " + gliederung + " (" + bildungsgangname + ") zugelassen, bzw. nicht angetreten:");

                    i = 1;

                    foreach (var schueler in eingeschulte)
                    {
                        if (!(from s in schuelerMitAbschluss where s.Id == schueler.Id select s).Any())
                        {
                            if (!(from s in wiederholerDerUnterstufe where s.Id == schueler.Id select s).Any())
                            {
                                if (!(from s in abgängerDerUnterstufe where s.Id == schueler.Id select s).Any())
                                {
                                    datei.Add(i.ToString().PadLeft(4) + ". " + (schueler.Nachname + ", ").PadRight(22) + schueler.Vorname);
                                    i++;
                                }
                            }
                        }
                    }
                    if (i == 1)
                    {
                        datei.Add(" -keine- ");
                    }

                    datei.Add("");

                    datei.Add("Ziel im Schuljahr " + jahrAbschluss + " der Gliederung " + gliederung + " (" + bildungsgangname + ") erreicht:");

                    i = 1;

                    foreach (var schueler in schuelerMitAbschluss)
                    {
                        datei.Add(i.ToString().PadLeft(4) + ". " + (schueler.Nachname + ", ").PadRight(22) + schueler.Vorname);
                        i++;
                    }
                    if (i == 1)
                    {
                        datei.Add(" -keine- ");
                    }

                    datei.Add("");
                    datei.Add("Wiederholer der Oberstufe " + jahrAbschluss + " der Gliederung " + gliederung + " (" + bildungsgangname + "):");

                    i = 1;

                    foreach (var schueler in wiederholerOberstufe)
                    {
                        datei.Add(i.ToString().PadLeft(4) + ". " + (schueler.Nachname + ", ").PadRight(22) + schueler.Vorname);
                        i++;
                    }
                    if (i == 1)
                    {
                        datei.Add(" -keine- ");
                    }
                }

                datei.Add("");
                

                using (StreamWriter outputFile = new StreamWriter("FHR-QUOTE.txt"))
                {
                    foreach (string line in datei)
                        outputFile.WriteLine(line);
                }
                try
                {
                    System.Diagnostics.Process.Start(@"C:\Program Files (x86)\Notepad++\Notepad++.exe", "FHR-QUOTE.txt");
                }
                catch (Exception)
                {
                    System.Diagnostics.Process.Start("Notepad.exe", "FHR-QUOTE.txt");
                }
                Console.WriteLine("Alles paletti. Beenden mit ANYKEY");
                Console.ReadKey();

            }
            catch (Exception ex)
            {
                datei.Add(ex.ToString());
            }
            finally
            {
                datei.Add("ANYKEY klicken, um zu beenden.");
                Console.ReadKey();
            }
        }
    }
}