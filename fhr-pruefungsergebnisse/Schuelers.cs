﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;

namespace fhr_pruefungsergebnisse
{
    internal class Schuelers : List<Schueler>
    {
        public Schuelers(
            string connectionStringAtlantis, 
            string jahrEinschulungWiederholerUnterstufe, 
            string jahrEinschulung, 
            string jahrAbschluss, 
            string jahrNachAbschluss)
        {
            using (OdbcConnection connection = new OdbcConnection(connectionStringAtlantis))
            {
                DataSet dataSet = new DataSet();

                OdbcDataAdapter schuelerAdapter = new OdbcDataAdapter(@"SELECT DBA.schue_sj.vorgang_schuljahr AS Schuljahr,
DBA.schueler.pu_id AS Id,
DBA.schueler.name_1 AS Nachname,
DBA.schueler.name_2 AS Vorname,
DBA.klasse.klasse AS Klasse,
DBA.schue_sj.s_jahrgang AS Jahrgang,
DBA.schue_sj.s_bildungsgang AS Bildungsgang,
DBA.schue_sj.vo_klasse,
DBA.schue_sj.s_klassenziel_erreicht AS Klassenzielerreicht,
DBA.schue_sj.s_wdhl_jahrgang,
DBA.schue_sj.s_klassenziel_erreicht_hz,
DBA.schue_sj.s_austritts_grund_bdlg AS AustrittsgrundBildungsgang,
DBA.schue_sj.s_ausb_ziel_erreicht_bdlg,
DBA.schue_sj.s_austritt_schulabschluss_bdlg,
DBA.schue_sj.s_berufs_nr AS Gliederung,
DBA.schluessel.aufloesung AS Bildungsgangname
FROM((DBA.schueler JOIN DBA.schue_sj ON DBA.schueler.pu_id = DBA.schue_sj.pu_id) JOIN DBA.klasse ON DBA.schue_sj.kl_id = DBA.klasse.kl_id) JOIN DBA.schluessel ON DBA.schue_sj.s_berufs_nr = DBA.schluessel.wert
WHERE DBA.schue_sj.s_bildungsgang = 'C03' AND(DBA.schue_sj.vorgang_schuljahr = '" + jahrEinschulungWiederholerUnterstufe + @"' OR DBA.schue_sj.vorgang_schuljahr = '" + jahrEinschulung + @"' OR DBA.schue_sj.vorgang_schuljahr = '" + jahrAbschluss + @"' OR DBA.schue_sj.vorgang_schuljahr = '" + jahrNachAbschluss + @"') AND schluessel.Kennzeichen = 'FACHKLASSENSCHL'
ORDER BY DBA.klasse.klasse ASC
", connection);

                Console.Write("Schüler in Atlantis ".PadRight(75, '.'));

                connection.Open();
                schuelerAdapter.Fill(dataSet, "DBA.schueler");

                foreach (DataRow theRow in dataSet.Tables["DBA.schueler"].Rows)
                {
                    var schueler = new Schueler();

                    schueler.Schuljahr = theRow["Schuljahr"] == null ? "" : theRow["Schuljahr"].ToString();
                    schueler.Id = theRow["Id"] == null ? -99 : Convert.ToInt32(theRow["Id"]);
                    schueler.Nachname = theRow["Nachname"] == null ? "" : theRow["Nachname"].ToString();
                    schueler.Vorname = theRow["Vorname"] == null ? "" : theRow["Vorname"].ToString();
                    schueler.Klasse = theRow["Klasse"] == null ? "" : theRow["Klasse"].ToString();
                    schueler.Jahrgang = theRow["Jahrgang"] == null ? "" : theRow["Jahrgang"].ToString(); // C031, C032
                    schueler.Bildungsgang = theRow["Bildungsgang"] == null ? "" : theRow["Bildungsgang"].ToString(); // C03
                    schueler.Bildungsgangname = theRow["Bildungsgangname"] == null ? "" : theRow["Bildungsgangname"].ToString(); // Sozial- und Ges.
                    schueler.Klassenzielerreicht = theRow["Klassenzielerreicht"] == null ? "" : theRow["Klassenzielerreicht"].ToString(); // C Ja in der Unterstufe; Null in der Oberstufe
                    schueler.AustrittsgrundBildungsgang = theRow["AustrittsgrundBildungsgang"] == null ? "" : theRow["AustrittsgrundBildungsgang"].ToString(); // 4H
                    schueler.Gliederung = theRow["Gliederung"] == null ? "" : theRow["Gliederung"].ToString(); // C0311200 , ...
                    this.Add(schueler);                    
                }
                Console.WriteLine((" " + this.Count.ToString()).PadLeft(30, '.'));
            }
        }

        internal List<Schueler> GetWiederholerDerOberstufe(string gliederung, string jahrAbschluss, string jahrNachAbschluss)
        {
            return (from s in this
                    where s.Jahrgang == "C032"
                    where s.Bildungsgang == "C03"
                    where s.Klasse.Length > 3
                    where s.Gliederung == gliederung
                    where s.AustrittsgrundBildungsgang != "4H"
                    where s.Schuljahr == jahrAbschluss
                    where ((from ss in this
                            where ss.Id == s.Id
                            where ss.Jahrgang == "C032"
                            where ss.Bildungsgang == "C03"
                            where ss.Klasse.Length > 3
                            where ss.Gliederung == gliederung
                            where ss.Schuljahr == jahrNachAbschluss
                            select ss
                           ).Any())
                    select s).ToList();
        }

        internal List<Schueler> GetWiederholerDerUnterstufe(string gliederung, string jahrEinschulung, string jahrAbschluss)
        {
            return (from s in this
                    where s.Jahrgang == "C031"
                    where s.Bildungsgang == "C03"
                    where s.Klasse.Length > 3
                    where s.Gliederung == gliederung
                    where s.Klassenzielerreicht != "C JA"
                    where s.Schuljahr == jahrEinschulung
                    where ((from ss in this
                            where ss.Id == s.Id
                            where ss.Jahrgang == "C031"
                            where ss.Bildungsgang == "C03"
                            where ss.Klasse.Length > 3
                            where ss.Gliederung == gliederung
                            where ss.Schuljahr == jahrAbschluss
                            select ss
                           ).Any())
                    select s).ToList();
        }

        internal List<Schueler> GetAbgängerDerUnterstufe(string gliederung, string jahrEinschulung, string jahrAbschluss)
        {
            return (from s in this
                    where s.Jahrgang == "C031"
                    where s.Bildungsgang == "C03"
                    where s.Klasse.Length > 3
                    where s.Gliederung == gliederung
                    where s.Klassenzielerreicht != "C JA"
                    where s.Schuljahr == jahrEinschulung
                    // Der Schüler ist im Folgejahr nicht erneut im Bildungsgang
                    where (!(from ss in this
                             where ss.Id == s.Id
                             where ss.Jahrgang == "C031"
                             where ss.Bildungsgang == "C03"
                             where ss.Klasse.Length > 3
                             where ss.Gliederung == gliederung
                             where ss.Schuljahr == jahrAbschluss
                             select ss
                           ).Any())
                    select s).ToList();
        }

        internal List<Schueler> GetEingeschluteWiederholer(string gliederung, string jahrEinschulung, string jahrEinschulungWiederholerUnterstufe)
        {
            return (from s in this
                    where s.Jahrgang == "C031"
                    where s.Bildungsgang == "C03"
                    where s.Klasse.Length > 3
                    where s.Gliederung == gliederung
                    where s.Schuljahr == jahrEinschulung
                    where ((from ss in this
                            where ss.Id == s.Id
                            where ss.Jahrgang == "C031"
                            where ss.Bildungsgang == "C03"
                            where ss.Klasse.Length > 3
                            where ss.Gliederung == gliederung
                            where ss.Schuljahr == jahrEinschulungWiederholerUnterstufe
                            select ss
                           ).Any())
                    select s).ToList();
        }

        internal List<Schueler> GetEingeschulte(string gliederung, string jahrEinschulung)
        {
            return (from s in this
                    where s.Jahrgang == "C031"
                    where s.Bildungsgang == "C03"
                    where s.Klasse.Length > 3
                    where s.Gliederung == gliederung
                    where s.Schuljahr == jahrEinschulung
                    select s).ToList();
        }

        internal List<Schueler> GetAbsolventen(string gliederung, string jahrAbschluss)
        {
            return (from s in this
                    where s.Jahrgang == "C032"
                    where s.Bildungsgang == "C03"
                    where s.Klasse.Length > 3
                    where s.Gliederung == gliederung
                    where s.AustrittsgrundBildungsgang == "4H"
                    where s.Schuljahr == jahrAbschluss
                    select s).ToList();
        }
    }
}